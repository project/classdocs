Class Docs
==========
Generates documentation about a class or set of classes defined by any
installed module.

Documentation may be generated in one of three forms:
- text: A textual listing of the class properties and methods, with
  types and signatures, prependend by the class Doxygen comments.
- html: Same content as the text listing but output as HTML, styled and
  presented in a table.
- graph: A DOT format file containing UML class diagrams suitable for input
  to the Graphviz graphing package.

Usage:
drush classdocs [--text | --html | --graph] [--sort]
                [--module=module1,...moduleN | --class=class1,...classN]

Options:
  - --class: Comma delimited list of classes to document.
  - --module: Comma delimited list of modules to document.
  - --text: Signifies text output is desired (default).
  - --html: Signifies HTML output is desired.
  - --graph: Signifies Graphviz output is desired.
  - --sort: Outputs class methods and properties in alphabetical order.


Installation
============
Install as normal. Enable the classdocs module. The classdocs command is now
available to drush.
