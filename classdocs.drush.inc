<?php

/**
 * @file
 * Generates documentation on a class or set of classes defined by a module.
 *
 * Documentation may be generated in one of three forms:
 * - text: A textual listing of the class properties and methods, with
 *   types and signatures, prependend by the class Doxygen comments.
 * - html: Same content as the text listing but output as HTML, styled and
 *   presented in a table.
 * - graph: A DOT format file containing UML class diagrams suitable for input
 *   to the Graphviz graphing package.
 *
 * Usage:
 * drush classdoc [--text | --html | --graph] [--sort]
 *                [--module=module1,...moduleN | --class=class1,...classN]
 *
 * Options:
 *   - --class: Comma delimited list of classes to document.
 *   - --module: Comma delimited list of modules to document.
 *   - --text: Signifies text output is desired (default).
 *   - --html: Signifies HTML output is desired.
 *   - --graph: Signifies Graphviz output is desired.
 *   - --sort: Outputs class methods and properties in alphabetical order.
 *
 * This is the Drush version of this script.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Implements hook_drush_help().
 *
 * Use for extensive help, more than just the short usage summary that
 * is already provided in hook_drush_command().
 */
function classdocs_drush_help($section) {
  switch ($section) {
    case 'drush:classdocs':
      return dt("Generates class documentation.");
  }
}

/**
 * Implements hook_drush_command().
 */
function classdocs_drush_command() {
  $items['classdocs'] = array(
    'description' => 'Print reference card of class names, properties, and methods.',
    'options' => array(
      'class'  => 'Comma delimited list of classes to document.',
      'module' => 'Comma delimited list of modules to document.',
      'text'   => 'Signifies text output is desired (default).',
      'html'   => 'Signifies HTML output is desired.',
      'graph'  => 'Signifies Graphviz output is desired.',
      'sort'   => 'Outputs class methods and properties in alphabetical order.',
    ),
    'aliases' => array('doc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'core' => array('7+'),
    'examples' => array(
      'drush classdocs --module=ups,usps' => 'Generate class documentation for all classes in the listed modules.',
      'drush classdocs --class=UPSCredentials,UPSXMLWriter' => 'Generate class documentation for just the specified classes.',
      'drush classdocs --module=UPS --graph' => 'Generate class documentation for just the specified classes.',
    ),
  );

  return $items;
}

/**
 * Implements drush_hook_COMMAND_validate().
 *
 * Requires user to specify either --module or --class.
 */
function drush_classdocs_validate() {
  $modules = drush_get_option('module');
  $classes = drush_get_option('class');
  if (empty($modules) && empty($classes)) {
    drush_set_error('DRUSH_CLASSDOCS_MISSING_ARG', dt("Usage: drush classdocs [--text | --html | --graph] [--module=module1,module2,...,moduleN | --class=class1,class2,...,classN]\n\nTry 'drush classdocs --help' for more information."));
  }
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_classdocs() {
  // Command arguments.
  //$modules = func_get_args();

  $modules = drush_get_option('module');
  $classes = drush_get_option('class');

  // Only one should be TRUE. If more than one is set, --text has highest
  // precedence, followed by --html, followed by --graph.
  $text = (drush_get_option('text')  != NULL);
  $html = !$text && (drush_get_option('html')  != NULL);
  $viz  = !$html && (drush_get_option('graph') != NULL);
  // $text is the default.
  $text = $text || (!$text && !$html && !$viz);

  $sort  = (drush_get_option('sort') != NULL);

  // Right now, if both --module and --class were specified, we ignore --class.
  // It would be nice to merge the query results so we could allow both
  // --module and --class. Can this be done simply with + ? No.
  if (!empty($modules)) {
    $modules = explode(',', $modules);
    // Find classes declared by the modules in the argument list.
    // JOIN on system table to get module description from info column.
    $query = db_select('registry', 'r');
    $query->join('system', 's', 'r.module = s.name');
    $result = $query
      ->fields('r', array('name', 'module'))
      ->fields('s', array('info'))
      ->condition('r.module', $modules, 'IN')
      ->execute();
  }
  elseif (!empty($classes)) {
    $classes = explode(',', $classes);
    // Find modules which provide the classes in the argument list.
    // JOIN on system table to get module description from info column.
    $query = db_select('registry', 'r');
    $query->join('system', 's', 'r.module = s.name');
    $result = $query
      ->fields('r', array('name', 'module'))
      ->fields('s', array('info'))
      ->condition('r.name', $classes, 'IN')
      ->execute();
  }

  // Array used to generate Graphviz DOT file.
  $graph = array();

  // Loop over all classes.
  foreach ($result as $record) {
    //drush_print(var_export($record, TRUE));

    // Assemble data for theme_table().
    $header = array();
    $rows   = array();

    $node_info = array();

    $reflector = new DrupalReflectionClass($record->name);

    // PHP 5.3
    //$cname = $reflector->getShortName(); // Without namespace.
    $cname = $reflector->getName();
    $node_info['title'] = $cname;

    // Class Doxygen comments.
    $doccomment = $reflector->getDocComment();
    if ($text) drush_print($doccomment);

    // To get hierarchy, recursively call getParentClass()
    // indent spaces = 2 * depth of of call
    // each parent gets a line until we have no parents.
//    $parentclass = $reflector->getParentClass();
//      if ($text) drush_print(trim(implode(' ', array(
//        $reflector->getModifierString(),
//        'class',
//        $cname,
//        'extends',
//        $parentclass->getName()
//      ))));

    $interfaces = $reflector->getInterfaceNames();
    if (!empty($interfaces)) {
      $interfaces = implode(' ', array(
        'implements',
        implode(', ', $interfaces),
      ));
    }

    $parentclass = $reflector->getParentClass();
    if ($parentclass) {
      if ($text) drush_print(trim(implode(' ', array(
        $reflector->getModifierString(),
        $reflector->isInterface() ? 'interface' : 'class',
        $cname,
        'extends',
        $parentclass->getName(),
        !empty($interfaces) ? $interfaces . ' {' : '{',
      ))));
      // Do we need to make a node for the parent?
      // What if the parent is in our group?  Then it's already taken care of.
      //$graph['nodes'][$parentclass->getName()] = $node_info;

      $header = array(
        array('data' => $cname, 'class' => 'classdocs-classname', 'colspan' => 2),
        array('data' => $parentclass->getName(), 'class' => 'classdocs-extends', 'colspan' => 2),
      );
      $graph['edges'][$cname][$parentclass->getName()] = array(
        'arrowhead' => 'empty',
        'headlabel' => '',
        'taillabel' => '',
        'style' => 'solid',
      );
      if (!empty($interfaces)) {
        foreach ($reflector->getInterfaceNames() as $interface) {
         $graph['edges'][$cname][$interface] = array(
           'arrowhead' => 'empty',
           'headlabel' => '',
           'taillabel' => '',
           'style' => 'dashed',
         );
        }
      }
    }
    else {
      if ($text) drush_print(trim(implode(' ', array(
        $reflector->getModifierString(),
        $reflector->isInterface() ? 'interface' : 'class',
        $cname,
        !empty($interfaces) ? $interfaces . ' {' : '{',
      ))));
      $header = array(
        array('data' => $reflector->isInterface() ? '<i>' . $cname . '</i>' : $cname, 'class' => 'classdocs-classname', 'colspan' => 2),
        array('data' => '', 'class' => 'classdocs-extends', 'colspan' => 2),
      );
    }
    // PHP 5.3
    if ($text) drush_print('class ' . $classname . ' | ' . $reflector->getNamespaceName());

    // Classname                          |   Namespace
    // Class Doxygen comments here?
    // Stick figure hierarchy, with class names fully qualified with namespace
    //                         iff they're not in the current namespace
    // Parent
    //   - Child
    //       - Child ......... Interface, Interface, Interface

    // Constructors
    //   access         type name
    $methods = $reflector->getMethods();
    if ($sort) {
      uasort($methods, 'compare_method_names');
    }
    foreach ($methods as $method) {
      // Only want methods declared or overridden by this subclass.
      if ($method->class == $cname &&
          $method->isConstructor()    ) {
        if ($text) drush_print('// Constructors', 2);
        $rows[] = array(
          '',
          array('data' => '// Constructors', 'class' => 'classdocs-section', 'colspan' => 3),
        );
        $list = $method->getParameterList();
        if ($text) drush_print(trim(implode(' ', array(
          $method->getModifierString(),
          $method->name,
          '(' . implode(', ', $list) . ');',
        ))), 2);
        $node_info['methods'][$method->name . '(' . implode(', ', $list) . ')'] = array();
        $rows[] = array(
          array('data' => $method->getModifierString(), 'colspan' => 1),
          array('data' => 'void', 'class' => 'classdocs-type'),
          array('data' => '<b>' . $method->name . '</b>' . '(' . implode(', ', $list) . ')', 'colspan' => 2),
        );
        if ($text) drush_print();
        $rows[] = array('', '', '', '');
      }
    }

    // Constants
    //   access         type name = value
    $constants = $reflector->getConstants();
    if (!empty($constants)) {
      if ($text) drush_print('// Constants', 2);
        $rows[] = array(
          '',
          array('data' => '// Constants', 'class' => 'classdocs-section', 'colspan' => 3),
        );
      foreach ($constants as $name => $value) {
        if ($text) drush_print(implode(' ', array(
          'const',
          $name,
          '=',
          $value . ';',
        )), 2);
        $rows[] = array(
          '',
          array('data' => 'const', 'class' => 'classdocs-type'),
          array('data' => '<b>' . $name . '</b>' . ' = ' . $value, 'colspan' => 2),
        );
      }
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }

    // Class Variables
    //   access         type name
    $properties = $reflector->getProperties(ReflectionProperty::IS_STATIC);
    $defaults   = $reflector->getDefaultProperties();
    $first = TRUE;
    if (!empty($properties)) {
      if ($sort) {
        uasort($properties, 'compare_property_names');
      }
      foreach ($properties as $property) {
        if ($property->class == $cname &&
            $property->isStatic() ) {
          if ($first) {
            if ($text) drush_print('// Class Variables', 2);
            $rows[] = array(
              '',
              array('data' => '// Class Variables', 'class' => 'classdocs-section', 'colspan' => 3),
            );
            $first = FALSE;
          }
          $default = $defaults[$property->getName()];
          $type = $reflector->getTypeName($default);
          $default_value = $reflector->getDefaultValue($default);
          $suffix  = '';
          if (isset($default_value)) {
            $suffix  = ' = ' . $default_value;
          }
          $pname   = $type . ' $' . $property->getName() . $suffix;
          if ($text) drush_print(trim(implode(' ', array(
            $property->getModifierString(),
            $pname . ';',
          ))), 2);
          $node_info['properties'][$property->getName()] = array(
            'type' => $type,
          );
          $rows[] = array(
            array('data' => $property->getModifierString(), 'colspan' => 1),
            array('data' => $type, 'class' => 'classdocs-type'),
            array('data' => '$' . '<b>' . $property->getName() . '</b>' . $suffix, 'colspan' => 2),
          );
        }
      }
    }
    if (!$first) {
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }

    // Class Methods
    //   access   returntype name (type arg, ..) throws exception
    $methods = $reflector->getMethods(ReflectionMethod::IS_STATIC);
    if ($sort) {
      uasort($methods, 'compare_method_names');
    }
    $first = TRUE;
    foreach ($methods as $method) {
      // Only want methods declared or overridden by this subclass.
      if ($method->class == $cname) {
        if ($first) {
          if ($text) drush_print('// Class Methods', 2);
          $rows[] = array(
            '',
            array('data' => '// Class Methods', 'class' => 'classdocs-section', 'colspan' => 3),
          );
            $first = FALSE;
          }
        $list = $method->getParameterList();
        if ($text) drush_print(trim(implode(' ', array(
          $method->getModifierString(),
          $method->name,
          '(' . implode(', ', $list) . ');',
        ))), 2);
        $node_info['methods'][$method->getName() . '(' . implode(', ', $list) . ')'] = array(
          // Can't determine return value for methods in PHP without some
          // metadata, which we don't have.
          //'type' => 'int',
        );
        $rows[] = array(
          array('data' => $method->getModifierString(), 'colspan' => 1),
          array('data' => '    ', 'class' => 'classdocs-type'),
          array('data' => '<b>' . $method->name . '</b>' . '(' . implode(', ', $list) . ')', 'colspan' => 2),
        );
      }
    }
    if (!$first) {
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }

    // Instance Variables
    //   access         type name
    $properties = $reflector->getProperties();
    $defaults   = $reflector->getDefaultProperties();
    $first = TRUE;
    if (!empty($properties)) {
      if ($sort) {
        uasort($properties, 'compare_property_names');
      }
      foreach ($properties as $property) {
        // Only want variables declared or overridden by this subclass.
        if ($property->class == $cname &&
            !$property->isStatic() ) {
          if ($first) {
            if ($text) drush_print('// Instance Variables', 2);
            $rows[] = array(
              '',
              array('data' => '// Instance Variables', 'class' => 'classdocs-section', 'colspan' => 3),
            );
            $first = FALSE;
          }
          // $default needs to be adjusted if = Array or NULL or string type.
          // just like we did in getParameterList().
          $default = $defaults[$property->getName()];
          $type = $reflector->getTypeName($default);
          $default_value = $reflector->getDefaultValue($default);
          $suffix  = '';
          if (isset($default_value)) {
            $suffix  = ' = ' . $default_value;
          }
          $pname   = $type . ' $' . $property->getName() . $suffix;
          if ($text) drush_print(trim(implode(' ', array(
            $property->getModifierString(),
            $pname . ';',
          ))), 2);
          $node_info['properties'][$property->getName()] = array(
            'type' => $type,
          );
          $rows[] = array(
            array('data' => $property->getModifierString(), 'colspan' => 1),
            array('data' => $type, 'class' => 'classdocs-type'),
            array('data' => '$' . '<b>' . $property->getName() . '</b>' . $suffix, 'colspan' => 2),
          );
        }
      }
    }
    if (!$first) {
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }

    // Instance Methods
    //   access   returntype name (type arg, ..) throws exception
    $methods = $reflector->getMethods();
    if ($sort) {
      uasort($methods, 'compare_method_names');
    }
    $first = TRUE;
    foreach ($methods as $method) {
      // Only want methods declared or overridden by this subclass.
      if ($method->class == $cname &&
          !$method->isStatic()     &&
          !$method->isConstructor()   ) {
        if ($first) {
          if ($text) drush_print('// Instance Methods', 2);
          $rows[] = array(
            '',
            array('data' => '// Instance Methods', 'class' => 'classdocs-section', 'colspan' => 3),
          );
          $first = FALSE;
        }
        $list = $method->getParameterList();
        if ($text) drush_print(trim(implode(' ', array(
          $method->getModifierString(),
          $method->name,
          '(' . implode(', ', $list) . ');',
        ))), 2);
        $node_info['methods'][$method->getName() . '(' . implode(', ', $list) . ')'] = array(
          // Can't determine return value for methods in PHP without some
          // metadata, which we don't have.
          //'type' => 'int',
        );
        $rows[] = array(
          array('data' => $method->getModifierString(), 'colspan' => 1),
          array('data' => '    ', 'class' => 'classdocs-type'),
          array('data' => '<b>' . $method->name . '</b>' . '(' . implode(', ', $list) . ')', 'colspan' => 2),
        );
      }
    }
    if (!$first) {
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }

    // Additional information ...
    //$filename = $reflector->getFileName();

    //Node not part of group.
    //$graph['nodes'][$cname] = $node_info;
    $module_title = unserialize($record->info);
    $graph['nodes']['cluster_' . $record->module]['label'] = $module_title['name'];
    $graph['nodes']['cluster_' . $record->module]['group'] = TRUE;
    $graph['nodes']['cluster_' . $record->module][$cname] = $node_info;
    // Check to see if we have anything to graph, or if a graph was requested.
    if ($html) {
      echo '<link rel="stylesheet" type="text/css" href="/' .
           drupal_get_path('module', 'classdocs') . 'css/classdocs.css' .
           '"/>' . "\n";
      echo theme('table', array(
        'header'    => $header,
        'rows'      => $rows,
      ));
    }
    if ($text) drush_print("}\n\n");
  }

  // Check to see if we have anything to graph, or if a graph was requested.
  if (!empty($graph) && $viz) {
    echo _drush_classdocs_generate($graph);
  }
}

/**
 * Taken from http://git.drupal.org/sandbox/damz/1438582.git without alteration.
 */
function _drush_classdocs_relationship(&$graph, $source, $target, $required, $cardinality) {
  $edge_info = array(
    'arrowhead' => 'normal',
  );
  if ($cardinality >= 1) {
    $min_cardinality = $required ? $cardinality : 0;
    $max_cardinality = $cardinality;
  }
  else {
    $min_cardinality = $required ? 1 : 0;
    $max_cardinality = '*';
  }
  $edge_info['taillabel'] = $min_cardinality . '..' . $max_cardinality;

  $edge_info['headlabel'] = '1..*';

  $graph['edges'][$source][$target] = $edge_info;
}

/**
 * Taken from http://git.drupal.org/sandbox/damz/1438582.git without alteration.
 */
function _drush_classdocs_generate($graph) {
  // Merge in defaults.
  $graph += array(
    'nodes' => array(),
    'edges' => array(),
  );

  $output = "digraph G {\n";
  $output .= "fontname = \"Bitstream Vera Sans\"\n";
  $output .= "fontsize = 8\n";

  $output .= "node [\n";
  $output .= "fontname = \"Bitstream Vera Sans\"\n";
  $output .= "fontsize = 8\n";
  $output .= "shape = \"record\"\n";
  $output .= "]\n";

  foreach ($graph['nodes'] as $name => $node_info) {
    if (!empty($node_info['group'])) {
      $output .= _drush_classdocs_generate_subgraph($name, $node_info);
    }
    else {
      $output .= _drush_classdocs_generate_node($name, $node_info);
    }
  }

  foreach ($graph['edges'] as $source_node => $edges) {
    foreach ($edges as $target_node => $edge_info) {
      $output .= "edge [\n";
      $output .= "fontname = \"Bitstream Vera Sans\"\n";
      $output .= "fontsize = 8\n";
      foreach ($edge_info as $k => $v) {
        $output .= ' "' . check_plain($k) . '" = "' . check_plain($v) . '"' . "\n";
      }
      $output .= "]\n";
      $output .= $source_node . ' -> ' . $target_node . "\n";
    }
  }

  $output .= "\n}\n";
  return $output;
}

/**
 * Taken from http://git.drupal.org/sandbox/damz/1438582.git without alteration.
 */
function _drush_classdocs_generate_subgraph($name, $subgraph_info) {
  $label = $subgraph_info['label'];
  unset($subgraph_info['label']);
  unset($subgraph_info['group']);

  $output = "subgraph $name {\n";
  $output .= 'label = "' . check_plain($label) . '"' . "\n";

  foreach ($subgraph_info as $node_name => $node_info) {
    $output .= _drush_classdocs_generate_node($node_name, $node_info);
  }

  $output .= "}\n";
  return $output;
}

/**
 * Taken from http://git.drupal.org/sandbox/damz/1438582.git.
 *
 * Minor alterations were made because I need to graph title, properties,
 * and methods instead of title, properties, and fields.
 */
function _drush_classdocs_generate_node($name, $node_info) {
  // Merge in defaults.
  $node_info += array(
    'title' => $name,
    'properties' => array(),
    'methods' => array(),
  );

  $label  = $node_info['title'] . '|';

  foreach ($node_info['properties'] as $property_name => $property_info) {
    $label .= $property_name;
    if (isset($property_info['type'])) {
      $label .= ' : ' . $property_info['type'];
    }
    $label .= '\l';
  }

  $label .= '|';

  foreach ($node_info['methods'] as $method_name => $method_info) {
    $label .= $method_name;
    if (isset($method_info['type'])) {
      $label .= ' : ' . $method_info['type'];
    }
    $label .= '\l';
  }

  return $name . ' [ label = "{' . check_plain($label) . '}" ]' . "\n";
}

/**
 *
 */
function compare_property_names(ReflectionProperty $a, ReflectionProperty $b) {
    if ($a->name == $b->name) {
        return 0;
    }
    return ($a->name < $b->name) ? -1 : 1;
}

/**
 *
 */
function compare_method_names(ReflectionMethod $a, ReflectionMethod $b) {
    if ($a->name == $b->name) {
        return 0;
    }
    return ($a->name < $b->name) ? -1 : 1;
}

/**
 * Output class variables.
 *
 * access         type name
 */
function class_variables(DrupalReflectionClass $reflector) {
    $cname = $reflector->getName();

    $properties = $reflector->getProperties(ReflectionProperty::IS_STATIC);
    $defaults   = $reflector->getDefaultProperties();
    $first = TRUE;
    if (!empty($properties)) {
      if ($sort) {
        uasort($properties, 'compare_property_names');
      }
      foreach ($properties as $property) {
        if ($property->class == $cname &&
            $property->isStatic() ) {
          if ($first) {
            if ($text) drush_print('// Class Variables', 2);
            $rows[] = array(
              '',
              array('data' => '// Class Variables', 'class' => 'classdocs-section', 'colspan' => 3),
            );
            $first = FALSE;
          }
          $default = $defaults[$property->getName()];
          $type = $reflector->getTypeName($default);
          $default_value = $reflector->getDefaultValue($default);
          $suffix  = '';
          if (isset($default_value)) {
            $suffix  = ' = ' . $default_value;
          }
          $pname   = $type . ' $' . $property->getName() . $suffix;
          if ($text) drush_print(trim(implode(' ', array(
            $property->getModifierString(),
            $pname . ';',
          ))), 2);
          $node_info['properties'][$property->getName()] = array(
            'type' => $type,
          );
          $rows[] = array(
            array('data' => $property->getModifierString(), 'colspan' => 1),
            array('data' => $type, 'class' => 'classdocs-type'),
            array('data' => '$' . '<b>' . $property->getName() . '</b>' . $suffix, 'colspan' => 2),
          );
        }
      }
    }
    if (!$first) {
      if ($text) drush_print();
      $rows[] = array('', '', '', '');
    }
}
