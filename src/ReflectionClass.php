<?php

/**
 * @file
 * Subclasses of PHP core Reflection API.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */

// Would be nice to have a ReflectionModule class so we can reflect all
// the free-range methods in a module.

namespace Drupal\classdocs;

/**
 * Extends PHP's built-in ReflectionClass to provide more capabilities.
 */
class ReflectionClass extends \ReflectionClass {

  /** Bitmask. */
  const IS_ALL = -1;

  /**
   * Overrides \ReflectionClass::getDefaultProperties().
   */
  public function getDefaultProperties() {
    $defaults = parent::getDefaultProperties();
    return $defaults;
    foreach ($defaults as $property => &$default) {
      if (is_array($default)) {
        $default = "array()";
      }
      elseif (is_null($default)) {
        $default = "NULL";
      }
      $default =  "'" . $value . "'";
    }
    return $defaults;
  }

  /**
   * Overrides \ReflectionClass::getProperties().
   */
  public function getProperties($filter = ReflectionClass::IS_ALL) {
    $result = array();
    $properties = parent::getProperties($filter);
    foreach ($properties as $property) {
      $result[] = self::getProperty($property->name);
    }
    return $result;
  }

  /**
   * Overrides \ReflectionClass::getProperty().
   */
  public function getProperty($name) {
    return new ReflectionProperty($this->name, $name);
  }

  /**
   * Overrides \ReflectionClass::getMethods().
   */
  public function getMethods($filter = ReflectionClass::IS_ALL) {
    $result = array();
    $methods = parent::getMethods($filter);
    foreach ($methods as $method) {
      $result[] = self::getMethod($method->name);
    }
    return $result;
  }

  /**
   * Overrides \ReflectionClass::getMethod().
   */
  public function getMethod($name) {
    return new ReflectionMethod($this->name, $name);
  }

  /**
   * Returns type for this parameter.
   *
   * @return
   *   Classname if type is a class, 'array' if type is an array,
   *   type name for any other type.
   */
  public function getTypeName($default) {
    $type = is_object($default) ? get_class($default) : gettype($default);
    $type = ($type == 'NULL') ? 'mixed' : $type;

    return $type;
  }

  /**
   * Formats parameter default value as found in code.
   */
  public function getDefaultValue($default) {
    if (!isset($default)) {
      return NULL;
    }
    if (is_null($default)) {
      return 'NULL';
    }
    if (is_array($default)) {
      return 'array()';
    }
    if (is_string($default)) {
      return "'" . $default . "'";
    }
    if (gettype($default) == 'boolean') {
      if ($default) {
        return 'TRUE';
      }
      return 'FALSE';
    }

    return $default;
  }

  /**
   * Formats modifier string as space-separated list, as found in code.
   */
  public function getModifierString() {
    return implode(' ', \Reflection::getModifierNames(parent::getModifiers()));
  }

}
