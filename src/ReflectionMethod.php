<?php

/**
 * @file
 * Subclasses of PHP core Reflection API.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */

namespace Drupal\classdocs;

/**
 * Extends PHP's built-in ReflectionMethod to provide more capabilities.
 */
class ReflectionMethod extends \ReflectionMethod {

  /**
   * Overrides \ReflectionMethod::getParameters().
   */
  public function getParameters() {
    $result = array();
    $parameters = parent::getParameters();
    foreach ($parameters as $parameter) {
      $result[] = new ReflectionParameter(array($this->class, $this->name), $parameter->name);
    }
    return $result;
  }

  /**
   * Formats parameter as found in code, with type hint and default value.
   */
  public function getParameterList() {
    $result = array();
    $parameters = self::getParameters();
    foreach ($parameters as $parameter) {
      $hint   = $parameter->getTypeHint();
      $hint   = empty($hint) ? $hint : $hint . ' ';
      $prefix = $parameter->isPassedByReference() ? '&$' : '$';
      $suffix = $parameter->isDefaultValueAvailable() ?
                ' = ' . $parameter->getDefaultValue() :
                '';
      $result[] = $hint . $prefix . $parameter->getName() . $suffix;
    }
    return $result;
  }

  /**
   * Formats modifier string as space-separated list, as found in code.
   */
  public function getModifierString() {
    return implode(' ', \Reflection::getModifierNames(parent::getModifiers()));
  }
}
