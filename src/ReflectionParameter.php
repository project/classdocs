<?php

/**
 * @file
 * Subclasses of PHP core Reflection API.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */

namespace Drupal\classdocs;

/**
 * Extends PHP's built-in ReflectionParameter to provide more capabilities.
 */
class ReflectionParameter extends \ReflectionParameter {

  /**
   * Overrides \ReflectionParameter::getDefaultValue().
   */
  public function getDefaultValue() {
    $value = parent::getDefaultValue();
    if (is_array($value)) {
      return "array()";
    }
    elseif (is_null($value)) {
      return "NULL";
    }

    return "'" . $value . "'";
  }

  /**
   * Returns type hint for this parameter, if present.
   *
   * @see http://docs.php.net/manual/en/reflectionclass.getmethod.php
   *
   * @return
   *   Classname if type is a class, or 'array' if type is an array.
   */
  public function getTypeHint() {
     if (parent::isArray()) {
       return 'array';
     }
     else {
       return parent::getClass()->name;
     }
  }
}
