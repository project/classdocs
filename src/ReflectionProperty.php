<?php

/**
 * @file
 * Subclasses of PHP core Reflection API.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */

namespace Drupal\classdocs;

/**
 * Extends PHP's built-in ReflectionProperty to provide more capabilities.
 */
class ReflectionProperty extends \ReflectionProperty {

  /**
   * Formats modifier string as space-separated list, as found in code.
   */
  public function getModifierString() {
    return implode(' ', \Reflection::getModifierNames(parent::getModifiers()));
  }
}
